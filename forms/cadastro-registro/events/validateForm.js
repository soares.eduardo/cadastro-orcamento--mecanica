function validateForm(form) {

    var msg = "";
    var estado = getValue("WKNumState");
    var indexes = form.getChildrenIndexes('listaPecas');

    if (estado == 4) {
        if (form.getValue("inputPlacaCarroOrcamento") == "") {
            msg += "Campo placa não foi preenchido";
        }

        if (form.getValue("inputProblema") == "") {
            msg += "Campo de problema não foi preenchido";
        }

        if (form.getValue("inputSolucao") == "") {
            msg += "Campo de solução não foi preenchido";
        }

        if(indexes.length < 1){
            msg += "Nenhum produto cadastrado"
        }else{
            for(var i = 0; i < indexes.length; i++){
                if (form.getValue("pecaPaiFilho___" + indexes[i]) == "") {
                    msg += "Produto não foi informado";
                }
                if(form.getValue("quantidadePaiFilho___" + indexes[i] == "")){
                    msg += "Quantidade não foi informado";
                }
                if(form.getValue("precoPaiFilho___" + indexes[i] == "")){
                    msg += "Preco não foi informado";
                }
            }
        }
        /*if (form.getValue("pecaPaiFilho") == "") {
            msg += "Campo de peça não foi preenchido";
        }

        if (form.getValue("quantidadePaiFilho") == "") {
            msg += "Campo de quantidade não foi preenchido";
        }*/
    }

    if(msg != ""){
        throw msg;
    }
}