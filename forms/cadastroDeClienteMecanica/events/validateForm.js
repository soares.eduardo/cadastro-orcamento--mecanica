function validateForm(form){

    var msg = "";

    if(form.getValue("inputNome") == ""){
        msg += "Campo nome não foi preenchido";
    }

    if(form.getValue("inputRg") == ""){
        msg += "Campo RG não foi preenchido";
    }

    if(form.getValue("inputCpf") == ""){
        msg += "Campo CPF não foi preenchido";
    }

    if(form.getValue("inputEndereco") == ""){
        msg += "Campo endereço não foi preenchido";
    }

    if(form.getValue("inputMarcaCarro") == ""){
        msg += "Campo da marca do carro não foi preenchido";
    }

    if(form.getValue("inputPlacaCarro") == ""){
        msg += "Campo da placa do carro não foi preenchido.";
    }

    if(msg != ""){
        throw msg;
    }

}